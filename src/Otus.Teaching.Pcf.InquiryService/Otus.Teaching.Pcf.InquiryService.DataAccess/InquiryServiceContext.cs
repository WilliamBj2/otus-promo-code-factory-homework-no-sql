﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.InquiryService.DataAccess
{
    public class InquiryServiceContext : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public InquiryServiceContext(DbContextOptions<InquiryServiceContext> options)
            : base(options)
        {
        }
    }
}
