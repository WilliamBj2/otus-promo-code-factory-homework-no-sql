﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Managers;
using Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using Otus.Teaching.Pcf.InquiryService.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.InquiryService.DataAccess.Managers
{
    public class UnitOfWork : IUnitOfWork
    {
        private Dictionary<Type, object> _dictionary;

        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
            _dictionary = new Dictionary<Type, object>();
        }

        public Task<IDbContextTransaction> BeginTransactionAsync()
        {
            throw new NotImplementedException();
        }

        public IRepository<T> GetRepository<T>() where T : BaseEntity
        {
            var type = typeof(T);
            var repo = _dictionary.TryGetValue(type, out object obj)
                ? obj as IRepository<T>
                : new Repository<T>(_context);

            if (obj is null)
                _dictionary.Add(type, repo);

            return repo;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
