﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Otus.Teaching.Pcf.InquiryService.DataAccess
{
    public class InquiryServiceContextFactory : IDesignTimeDbContextFactory<InquiryServiceContext>
    {
        public InquiryServiceContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<InquiryServiceContext>();
            optionsBuilder.UseNpgsql(
                "Host=localhost;Port=5432;Database=InquiryService;Username=WilliamBj2;Password=TeddyBeer97"
            );

            return new InquiryServiceContext(optionsBuilder.Options);
        }
    }
}
