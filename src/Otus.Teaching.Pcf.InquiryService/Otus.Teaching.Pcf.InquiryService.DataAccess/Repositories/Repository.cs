﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination;
using Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.InquiryService.DataAccess.Repositories
{
    public class Repository<T_src> : IRepository<T_src>
        where T_src : BaseEntity
    {
        private DbSet<T_src> _dbSet;

        public Repository(DbContext context)
        {
            _dbSet = context.Set<T_src>();
        }

        private IQueryable<T_src> IncludeProcessor(
            IQueryable<T_src> query,
            ICollection<Expression<Func<T_src, object>>> navs
        )
        {
            return navs.Any()
                ? navs.Aggregate(query, (_query, _next) => _query.Include(_next))
                : query;
        }

        private IQueryable<T_dst> OrderProcessor<T_dst>(
            IQueryable<T_dst> query,
            IEnumerable<OrderSelector> orderSelectors
        )
            where T_dst : class
        {
            if (orderSelectors.Any())
            {
                var type = typeof(T_dst);
                var parameter = Expression.Parameter(type, "o");

                return orderSelectors.Aggregate(query, (_query, _key) =>
                {
                    var lambda = Expression.Lambda(
                        Expression.Property(parameter, _key.ParamName), parameter);

                    Expression queryExpr = Expression.Call(
                        typeof(Queryable),
                        _key.OrderType == OrderType.Asc ? "OrderBy" : "OrderByDescending",
                        lambda.Type.GenericTypeArguments,
                        query.Expression, lambda);

                    return query.Provider.CreateQuery<T_dst>(queryExpr);
                });
            }

            return query;
        }

        private IQueryable<T_dst> PaginationProcessor<T_dst>(
            IQueryable<T_dst> query,
            int pageSize,
            int pageNumber
        )
            where T_dst : class
        {
            return query.Skip(pageSize * pageNumber).Take(pageSize);
        }

        private IQueryable<T_dst> FilterProcessor<T_dst>(
            IQueryable<T_src> query,
            IFilter<T_src> filter
        )
            where T_dst : class
        {
            // в этот момент должен выполняться projectTo(), но пока заглушим Cast()'ом
            var subQuery = query.Where(filter.Predicate).Cast<T_dst>();

            if (filter is IFilterWithOrdering<T_src, T_dst> fo)
            {
                subQuery = OrderProcessor(subQuery, fo.OrderSelectors);
                if (fo is IFilterWithPagination<T_src, T_dst> fp)
                {
                    subQuery = PaginationProcessor(subQuery, fp.PageSize, fp.PageNumber);
                }
            }

            return subQuery;
        }

        public async Task<ICollection<T_dst>> GetByFilterAsync<T_dst>(
            IFilter<T_src> filter,
            bool asNoTracking = true,
            params Expression<Func<T_src, object>>[] navs
        )
            where T_dst : class
        {
            var query = asNoTracking ? _dbSet.AsNoTracking() : _dbSet;
            query = IncludeProcessor(query, navs);
            return await FilterProcessor<T_dst>(query, filter).ToListAsync();
        }

        public void Create(T_src entity) => _dbSet.Add(entity);

        public void CreateRange(ICollection<T_src> entities) => _dbSet.AddRange(entities);
    }
}
