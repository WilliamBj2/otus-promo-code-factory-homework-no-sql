﻿
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System.Collections;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.InquiryService.WebHost.Maintenance.Converters
{
    public class QueryCollectionConverter<T> : IModelBinder
        where T : IEnumerable
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var fieldName = bindingContext.FieldName;
            var valueProviderResult =
                bindingContext.ValueProvider.GetValue(fieldName);
            
            var prep = $"[{string.Join(",", valueProviderResult)}]";
            bindingContext.Result =
                ModelBindingResult.Success(JsonConvert.DeserializeObject<T>(prep));

            return Task.CompletedTask;
        }
    }
}
