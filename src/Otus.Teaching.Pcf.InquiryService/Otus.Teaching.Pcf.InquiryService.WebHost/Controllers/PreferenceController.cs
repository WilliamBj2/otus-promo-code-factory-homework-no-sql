﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Managers;
using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination;
using Otus.Teaching.Pcf.InquiryService.WebHost.Models.Sorting_Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.InquiryService.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDistributedCache _distributedCache;

        public PreferenceController(IUnitOfWork unitOfWork, IDistributedCache distributedCache)
        {
            _unitOfWork = unitOfWork;
            _distributedCache = distributedCache;
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Preference>> GetByIdAsync([FromRoute(Name = "id")] Guid Id)
        {
            Preference result = null;

            var cached = await _distributedCache.GetStringAsync(Id.ToString());
            if (cached is not null)
            {
                result = JsonConvert.DeserializeObject<Preference>(cached);
            }
            else
            {
                var filter = new Filter<Preference>(x => x.Id == Id);
                var collection = await _unitOfWork
                    .GetRepository<Preference>()
                    .GetByFilterAsync<Preference>(filter);

                result = collection.FirstOrDefault();
                if (result is not null)
                {
                    await _distributedCache.SetStringAsync(
                        result.Id.ToString(),
                        JsonConvert.SerializeObject(result),
                        new DistributedCacheEntryOptions
                        {
                            SlidingExpiration = TimeSpan.FromSeconds(30)
                        }
                    );
                }
            }

            return result is not null ? Ok(result) : NotFound();
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<Preference>>>
            GetAllAsync([FromQuery] WithPagination pFilter)
        {
            var filter = new FilterWithPagination<Preference>
            (
                x => true,
                pFilter.OrderSelectors,
                pFilter.PageSize,
                pFilter.PageNumber
            );

            var result = await _unitOfWork
                .GetRepository<Preference>()
                .GetByFilterAsync<Preference>(filter);

            return Ok(result);
        }
    }
}
