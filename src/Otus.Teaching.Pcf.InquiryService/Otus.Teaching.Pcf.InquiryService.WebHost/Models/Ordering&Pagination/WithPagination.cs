﻿namespace Otus.Teaching.Pcf.InquiryService.WebHost.Models.Sorting_Pagination
{
    public interface IWithPagination
    {
        public int PageSize { get; init; }

        public int PageNumber { get; init; }
    }

    public class WithPagination : WithOrdering, IWithPagination
    {
        public int PageSize { get; init; }

        public int PageNumber { get; init; }
    }

    public class WithPagination<TFilter> : WithOrdering<TFilter>, IWithPagination
        where TFilter : class
    {
        public int PageSize { get; init; }

        public int PageNumber { get; init; }
    }
}
