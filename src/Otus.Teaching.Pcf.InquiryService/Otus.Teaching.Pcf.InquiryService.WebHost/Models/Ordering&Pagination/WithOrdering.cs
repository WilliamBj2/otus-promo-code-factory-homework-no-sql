﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination;
using Otus.Teaching.Pcf.InquiryService.WebHost.Maintenance.Converters;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.InquiryService.WebHost.Models.Sorting_Pagination
{
    public interface IWithOrdering
    {
        public IEnumerable<OrderSelector> OrderSelectors { get; init; }
    }

    public class WithOrdering : IWithOrdering
    {
        [ModelBinder(BinderType = typeof(QueryCollectionConverter<IEnumerable<OrderSelector>>), Name = nameof(OrderSelectors))]
        public IEnumerable<OrderSelector> OrderSelectors { get; init; }
    }

    public class WithOrdering<TFilter> : IWithOrdering
        where TFilter : class
    {
        public TFilter RequestModel { get; init; }

        public IEnumerable<OrderSelector> OrderSelectors { get; init; }
    }
}
