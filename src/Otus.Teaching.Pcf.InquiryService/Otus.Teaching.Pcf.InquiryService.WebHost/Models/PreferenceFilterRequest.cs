﻿using System;

namespace Otus.Teaching.Pcf.InquiryService.WebHost.Models
{
    public class PreferenceFilterRequest
    {
        public Guid Id { get; set; }
    }
}
