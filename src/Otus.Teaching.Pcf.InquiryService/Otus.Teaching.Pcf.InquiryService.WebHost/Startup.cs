using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Managers;
using Otus.Teaching.Pcf.InquiryService.DataAccess;
using Otus.Teaching.Pcf.InquiryService.DataAccess.Managers;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Otus.Teaching.Pcf.InquiryService.WebHost
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "pref_";
            });

            services.AddDbContext<DbContext, InquiryServiceContext>(opt =>
            {
                var connection = _configuration.GetConnectionString("InquiryServiceDB");
                opt.UseNpgsql(connection);
            });
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddControllers();

            services.AddSwaggerGen(options =>
            {
                var currentAssembly = Assembly.GetExecutingAssembly();
                var currentAssemblyLocation = Path.GetDirectoryName(currentAssembly.Location)!;

                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "InquiryService",
                    Version = currentAssembly.GetName().Version?.ToString(),
                    Description = string.Empty
                });

                var xmlDocs = currentAssembly.GetReferencedAssemblies()
                    .Union(new[] { currentAssembly.GetName() })
                    .Select(assembly => Path.Combine(currentAssemblyLocation, $"{assembly.Name}.xml"))
                    .Where(filePath => File.Exists(filePath))
                    .ToArray();

                Array.ForEach(xmlDocs, filePath => { options.IncludeXmlComments(filePath); });

                options.UseInlineDefinitionsForEnums();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "InquiryService v1");
                    options.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
