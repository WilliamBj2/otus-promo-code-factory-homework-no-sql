﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.InquiryService.Core.Domain
{
    public class Preference : BaseEntity
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}
