﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.InquiryService.Core.Domain
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
