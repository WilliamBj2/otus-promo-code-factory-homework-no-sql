﻿using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination
{
    public abstract class IFilterWithOrdering<T_src, T_dst> : IFilter<T_src>
        where T_src : BaseEntity
        where T_dst : class
    {
        public readonly IEnumerable<OrderSelector> OrderSelectors;

        protected IFilterWithOrdering(
            Expression<Func<T_src, bool>> predicate, IEnumerable<OrderSelector> orderSelectors
        )
            : base(predicate)
        {
            OrderSelectors = orderSelectors;
        }
    }

    public class FilterWithOrdering<T_src> : IFilterWithOrdering<T_src, T_src>
        where T_src : BaseEntity
    {
        public FilterWithOrdering(
            Expression<Func<T_src, bool>> predicate, IEnumerable<OrderSelector> orderSelectors
        )
            : base(predicate, orderSelectors)
        {
        }
    }

    public class FilterWithOrdering<T_src, T_dst> : IFilterWithOrdering<T_src, T_dst>
        where T_src : BaseEntity
        where T_dst : class
    {
        public FilterWithOrdering(
            Expression<Func<T_src, bool>> predicate, IEnumerable<OrderSelector> orderSelectors
        )
            : base(predicate, orderSelectors)
        {
        }
    }
}
