﻿using Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination.Enums;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination
{
    public class OrderSelector
    {
        [Required(AllowEmptyStrings = false)]
        public string ParamName { get; set; }

        [Required]
        public OrderType OrderType { get; set; }
    }
}
