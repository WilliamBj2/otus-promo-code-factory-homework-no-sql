﻿namespace Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination.Enums
{
    public enum OrderType { Asc = 0, Desc }
}
