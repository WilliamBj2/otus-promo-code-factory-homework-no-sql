﻿using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using System;
using System.Linq.Expressions;

namespace Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination
{
    public abstract class IFilter<T_src>
        where T_src : BaseEntity
    {
        public readonly Expression<Func<T_src, bool>> Predicate;

        protected IFilter(Expression<Func<T_src, bool>> predicate)
        {
            Predicate = predicate;
        }
    }

    public class Filter<T_src> : IFilter<T_src>
        where T_src : BaseEntity
    {
        public Filter(Expression<Func<T_src, bool>> predicate)
            : base(predicate)
        {
        }
    }
}
