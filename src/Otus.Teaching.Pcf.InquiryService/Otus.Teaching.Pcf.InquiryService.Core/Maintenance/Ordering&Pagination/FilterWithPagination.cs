﻿using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination
{
    public abstract class IFilterWithPagination<T_src, T_dst> : IFilterWithOrdering<T_src, T_dst>
        where T_src : BaseEntity
        where T_dst : class
    {
        public readonly int PageSize, PageNumber;

        protected IFilterWithPagination(
            Expression<Func<T_src, bool>> predicate,
            IEnumerable<OrderSelector> orderSelectors,
            int pageSize,
            int pageNumber
        )
            : base(predicate, orderSelectors)
        {
            PageSize = pageSize;
            PageNumber = pageNumber;
        }
    }

    public class FilterWithPagination<T_src> : IFilterWithPagination<T_src, T_src>
        where T_src : BaseEntity
    {
        public FilterWithPagination(
            Expression<Func<T_src, bool>> predicate,
            IEnumerable<OrderSelector> orderSelectors,
            int pageSize,
            int pageNumber
        )
            : base(predicate, orderSelectors, pageSize, pageNumber)
        {
        }
    }

    public class FilterWithPagination<T_src, T_dst> : IFilterWithPagination<T_src, T_dst>
        where T_src : BaseEntity
        where T_dst : class
    {
        public FilterWithPagination(
            Expression<Func<T_src, bool>> predicate,
            IEnumerable<OrderSelector> orderSelectors,
            int pageSize,
            int pageNumber
        )
            : base(predicate, orderSelectors, pageSize, pageNumber)
        {
        }
    }
}
