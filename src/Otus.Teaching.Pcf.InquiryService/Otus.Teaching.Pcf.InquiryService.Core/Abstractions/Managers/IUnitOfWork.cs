﻿using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Managers
{
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : BaseEntity;

        public Task<IDbContextTransaction> BeginTransactionAsync();

        public Task<int> SaveChangesAsync();
    }
}
