﻿using Otus.Teaching.Pcf.InquiryService.Core.Domain;
using Otus.Teaching.Pcf.InquiryService.Core.Maintenance.Sorting_Pagination;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Pcf.InquiryService.Core.Abstractions.Repositories
{
    public interface IRepository<T_src>
        where T_src : BaseEntity
    {
        public Task<ICollection<T_dst>> GetByFilterAsync<T_dst>(
            IFilter<T_src> filter,
            bool asNoTracking = true,
            params Expression<Func<T_src, object>>[] navs
        )
        where T_dst : class;

        public void Create(T_src entity);

        public void CreateRange(ICollection<T_src> entities);
    }
}
